package com.example.recycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
    }

    fun initRecycler(){
        val recycler:RecyclerView = findViewById(R.id.recycler_pokemons)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = PokemonAdapter(Pokemons.pokemonList)
    }
}