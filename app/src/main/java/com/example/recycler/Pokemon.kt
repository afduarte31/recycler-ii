package com.example.recycler

import android.media.Image
import androidx.annotation.DrawableRes

data class Pokemon(
    val id: Long,
    val name: String,
    val type: String,
    @DrawableRes
    val image: Int?
)