package com.example.recycler

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PokemonViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    val pokemonName = view.findViewById<TextView>(R.id.tvNombre)
    val pokemonNro = view.findViewById<TextView>(R.id.tvNro)
    val pokemonType = view.findViewById<TextView>(R.id.tvTipo)

    val pokemonPic = view.findViewById<ImageView>(R.id.imageView)

    fun show(pokemonModel: Pokemon){
        pokemonName.text = pokemonModel.name
        pokemonNro.text = pokemonModel.id.toString()
        pokemonType.text = pokemonModel.type

        pokemonModel.image?.let { pokemonPic.setImageResource(it) }
    }
}