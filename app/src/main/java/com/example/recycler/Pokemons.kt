package com.example.recycler

class Pokemons {
    companion object{
        val pokemonList = listOf<Pokemon>(
            Pokemon(
                id = 1,
                name = "Bulbasaur",
                type = "Plant",
                image = R.drawable.pk_001
            ),
            Pokemon(
                id = 2,
                name = "Ivysaur",
                type = "Plant",
                image = R.drawable.pk_002
            ),
            Pokemon(
                id = 3,
                name = "Venusaur",
                type = "Plant",
                image = R.drawable.pk_003
            ),
            Pokemon(
                id = 4,
                name = "Charmander",
                type = "Fire",
                image = R.drawable.pk_004
            ),
            Pokemon(
                id = 5,
                name = "Charmaleon",
                type = "Fire",
                image = R.drawable.pk_005
            ),
            Pokemon(
                id = 6,
                name = "Charizard",
                type = "Fire",
                image = R.drawable.pk_006
            ),
            Pokemon(
                id = 7,
                name = "Squirtle",
                type = "Water",
                image = R.drawable.pk_007
            ),
            Pokemon(
                id = 8,
                name = "Wartortle",
                type = "Water",
                image = R.drawable.pk_008
            ),
            Pokemon(
                id = 9,
                name = "Blastoise",
                type = "Water",
                image = R.drawable.pk_009
            ),
        )
    }
}